export type TSelectOption = {
	label: string;
	value: string | number;
};

export type TSingleSelectProps = {
	multiple?: false;
	value?: TSelectOption | undefined;
	onChange: (value: TSelectOption | undefined) => void;
};

export type TMultipleSelectProps = {
	multiple: true;
	value: TSelectOption[];
	onChange: (value: TSelectOption[]) => void;
};

export type TSelectProps = {
	options: TSelectOption[];
} & (TSingleSelectProps | TMultipleSelectProps);
