import React, { useState } from 'react';
import Select from '../components/Select';
import { TSelectOption } from '../types/select.types';

function App() {
	const options: TSelectOption[] = [
		{ label: 'First', value: 1 },
		{ label: 'Second', value: 2 },
		{ label: 'Third', value: 3 },
		{ label: 'Fourth', value: 4 },
		{ label: 'Fifth', value: 5 },
	];
	
	const [value1, setValue1] = useState<TSelectOption | undefined>(options[0]);
	const [value, setValue] = useState<TSelectOption[]>([]);

	const onChange1 = (option: TSelectOption | undefined) => {
		setValue1(option);
	};
	const onChange = (option: TSelectOption[]) => {
		setValue(option);
	};

	return (
		<>
			<Select options={options} onChange={onChange1} value={value1} />
			<Select options={options} onChange={onChange} value={value} multiple={true} />
		</>
	);
}

export default App;
