import React, { useEffect, useRef, useState } from 'react';
import { TSelectOption, TSelectProps } from '../types/select.types';
import styles from './select.module.css';

export default function Select({ onChange, options, value, multiple }: TSelectProps) {
	const [isOpen, setIsOpen] = useState<boolean>(false);
	const [highlightedIndex, setHighlightedIndex] = useState(0);

	const containerRef = useRef<HTMLDivElement>(null);

	function isOptionSelected(option: TSelectOption) {
		if (multiple) return value.map(e => JSON.stringify(e)).includes(JSON.stringify(option));
		return JSON.stringify(value) === JSON.stringify(option);
	}

	function selectOption(option: TSelectOption) {
		if (multiple) {
			if (isOptionSelected(option)) {
				onChange((value.map(x => JSON.stringify(x)).filter(o => o !== JSON.stringify(option))).map(x => JSON.parse(x)));
			} else {
				onChange([...value, option]);
			}
		} else {
			if (!isOptionSelected(option)) onChange(option);
		}
	}

	useEffect(() => {
		if (isOpen) setHighlightedIndex(0);
	}, [isOpen]);

	useEffect(() => {
		const handler = (e: KeyboardEvent) => {
			if (e.target != containerRef.current) return;
			switch (e.code) {
			case 'Enter':
			case 'Space':
				setIsOpen(prev => !prev);
				if (isOpen) selectOption(options[highlightedIndex]);
				break;
			case 'ArrowUp':
			case 'ArrowDown':
				if (!isOpen) {
					setIsOpen(true);
					break;
				}

				{
					const newValue = highlightedIndex + (e.code === 'ArrowDown' ? 1 : -1);
					if (newValue >= 0 || newValue < options.length) setHighlightedIndex(newValue);
				}
				break;
			case 'Escape':
				setIsOpen(false);
				break;
			default:
				break;
			}
		};
		containerRef.current?.addEventListener('keydown', handler);
		return () => containerRef.current?.removeEventListener('keydown', handler);
	}, [isOpen, highlightedIndex]);

	function clearOption() {
		multiple ? onChange([]) : onChange(undefined);
	}

	return (
		<div
			ref={containerRef}
			className={styles.container}
			tabIndex={0}
			onClick={() => (setIsOpen(!isOpen))}
			onBlur={() => (setIsOpen(false))}
		>
			<span className={styles.value}>{multiple ? value.map(v => (
				<button
					key={v.value}
					onClick={(e) => {
						e.stopPropagation();
						selectOption(v);
					}}
					className={styles['option-badge']}
				>
					{v.label}
					<span className={styles['remove-btn']}>&times;</span>
				</button>
			)) : value?.label}</span>
			<button
				className={styles['clear-btn']}
				onClick={(e) => {
					e.stopPropagation();
					clearOption();
				}}
			>
				&times;
			</button>
			<div className={styles.divider}></div>
			<div className={styles.caret}></div>
			<ul className={`${styles.options} ${isOpen ? styles.show : ''}`}>
				{
					options.map((option, index) => (
						<li
							key={option.value}
							onClick={(e) => {
								e.stopPropagation();
								selectOption(option);
								setIsOpen(false);
							}}
							onMouseEnter={() => {
								setHighlightedIndex(index);
							}}
							className={`${styles.option} ${isOptionSelected(option) ? styles.selected : ''
							} ${index === highlightedIndex ? styles.highlighted : ''
							}`}
						>
							{option.label}
						</li>
					))
				}
			</ul>
		</div>
	);
}
